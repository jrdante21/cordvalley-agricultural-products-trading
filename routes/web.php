<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\MemberController;
use App\Http\Controllers\API\MemberPayoutController;
use App\Http\Controllers\API\MemberPayoutRequestController;
use App\Http\Controllers\API\MemberProfitController;
use App\Http\Controllers\API\CodeController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\ProductItemController;
use App\Http\Controllers\API\PurchaseController;
use App\Http\Controllers\API\AdminUserController;
use App\Http\Controllers\API\AdminController;

use App\Http\Controllers\Member\UserController;
use App\Http\Controllers\Member\PurchaseController as UserPurchase;
use App\Http\Controllers\Member\PayoutRequestController as UserPayoutRequest;

use App\Http\Controllers\Auth\AdminController as AdminAuth;
use App\Http\Controllers\Auth\MemberController as MemberAuth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'site.home');
Route::view('/products', 'site.products');
Route::view('/marketing', 'site.marketing');
Route::view('/about-us', 'site.about-us');
Route::view('/register', 'site.register');
Route::view('/privacy-policy', 'site.privacy-policy');
Route::view('/login', 'site.login')->name('login');

// Member Route

Route::prefix('member')->name('member.')->group(function(){
    Route::post('/login', [MemberAuth::class, 'login']);
    Route::get('/logout', [MemberAuth::class, 'logout']);
    Route::get('/{any?}', [UserController::class, 'index'])->middleware('auth')->where('any','.*')->name('panel');
});

Route::prefix('api-member')->middleware(['auth', 'member'])->group(function(){
    Route::get('/profits', MemberProfitController::class);
    Route::get('/purchases', UserPurchase::class);
    Route::resource('request-payout', UserPayoutRequest::class)->only(['index','store']);
    Route::post('/change-password', [UserController::class, 'updatePassword']);
    Route::post('/edit-username', [UserController::class, 'updateUsername']);
});

// Admin Route

Route::prefix('capt-admin')->name('admin.')->group(function(){
    Route::get('/', [AdminAuth::class, 'index'])->name('login');
    Route::get('/logout', [AdminAuth::class, 'logout']);
    Route::post('/login', [AdminAuth::class, 'login']);
    Route::view('/panel/{any?}', 'admin')->middleware('auth:admin')->where('any','.*')->name('panel');
});

Route::prefix('api-admin')->middleware('auth:admin')->name('adminapi.')->group(function(){
    Route::get('/member-profits', MemberProfitController::class);
    Route::apiResource('members', MemberController::class);
    Route::apiResource('members.payouts', MemberPayoutController::class)->only(['store']);
    Route::apiResource('members.payout-requests', MemberPayoutRequestController::class)->except(['show'])->shallow();
    Route::apiResource('member-payouts', MemberPayoutController::class)->except(['store']);
    Route::apiResource('codes', CodeController::class);
    Route::apiResource('products', ProductController::class);
    Route::apiResource('products.items', ProductItemController::class)->only(['store']);
    Route::apiResource('product-items', ProductItemController::class)->except(['store']);
    Route::apiResource('purchases', PurchaseController::class);
    Route::apiResource('admins', AdminController::class);

    // Admin User
    Route::post('/admin-activate/{admin}', [AdminController::class, 'restore']);
    Route::post('/admin-profile', [AdminUserController::class, 'updateProfile']);
    Route::post('/admin-password', [AdminUserController::class, 'updatePassword']);
});