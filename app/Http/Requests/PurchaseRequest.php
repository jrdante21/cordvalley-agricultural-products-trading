<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'member_id' => 'required|numeric|exists:members,id',
            'remarks' => 'required|min:5',
            'ar_number' => 'required|min:3',
            'purchased_at' => 'required|date|before_or_equal:'.date('Y-m-d'),
            'purchase_items' => 'required|array',
            'purchase_items.*.product_id' => 'required|numeric|exists:product_items,id',
            'purchase_items.*.quantity' => 'required|numeric',
        ];
    }

    public function attributes()
    {
        return [
            'purchased_at' => 'date purchased'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'purchased_at' => date('Y-m-d', strtotime($this->purchased_at)),
        ]);
    }
}
