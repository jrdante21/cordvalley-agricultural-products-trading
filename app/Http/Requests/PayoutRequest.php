<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CheckAvailableBalance;
use App\Models\MemberPayout;

class PayoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $member = ($this->routeIs('adminapi.*')) ? $this->route('member') : auth()->user();

        return [
            'profits' => ['required', 'numeric', 'min:1000', new CheckAvailableBalance($member)],
            'remarks' => 'required|min:5',
        ];
    }

    public function withValidator($validator)
    {
        $member = ($this->routeIs('adminapi.*')) ? $this->route('member') : auth()->user();

        $payouts = MemberPayout::where('member_id', $member->id)->whereNull('confirmed_at')->count();
        $validator->after(function ($validator) use ($payouts) {
            if ($payouts >= 1) {
                $validator->errors()->add('pending_requests', 'Pending payout requests still not confirmed!');
            }
        });
    }
}
