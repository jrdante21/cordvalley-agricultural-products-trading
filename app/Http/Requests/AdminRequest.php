<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\AlphaSpace;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $admin = $this->route('admin');

        return [
            'fname' => ['required', 'min:2', new AlphaSpace],
            'mname' => ['required', 'min:2', new AlphaSpace],
            'lname' => ['required', 'min:2', new AlphaSpace],
            'username' => ['required', 'min:5', 'alpha_num', Rule::unique('admins', 'username')->ignore($admin)],
            'edit_password' => ['required', 'boolean'],
            'password' => ['exclude_if:edit_password,false', 'required', 'min:5'],
        ];
    }

    protected function prepareForValidation()
    {
        $admin = $this->route('admin');
        $this->merge([
            'edit_password' => (!empty($admin) && empty($this->password)) ? false : true
        ]);
    }
}
