<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\AlphaSpace;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('member');

        return [
            'fname' => ['required', 'min:2', new AlphaSpace],
            'mname' => ['required', 'min:2', new AlphaSpace],
            'lname' => ['required', 'min:2', new AlphaSpace],
            'suffix' => ['nullable', 'min:2', new AlphaSpace],
            'contact_number' => ['nullable', 'numeric'],
            'gender' => ['required', 'numeric'],
            'bday' => ['required', 'before_or_equal:'.date('Y-01-01', strtotime('-15 years'))],
            'barangay' => ['required', 'min:2'],
            'municipality' => ['required', 'min:2'],
            'province' => ['required', 'min:2'],
            'username' => ['required', 'alpha_num', 'min:5', Rule::unique('members', 'username')->ignore($id)],
            'password' => ['sometimes', 'required', 'min:5']
        ];
    }

    public function attributes()
    {
        return [
            'fname' => 'first name',
            'mname' => 'middle name',
            'lname' => 'last name',
            'bday' => 'birthdate'
        ];
    }

    public function messages()
    {
        return [
            'bday.before_or_equal' => "The birthdate must be 15 years below.",
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'bday' => date('Y-m-d', strtotime($this->bday)),
            'barangay' => strtoupper($this->barangay),
            'municipality' => strtoupper($this->municipality),
            'province' => strtoupper($this->province),
        ]);
    }
}
