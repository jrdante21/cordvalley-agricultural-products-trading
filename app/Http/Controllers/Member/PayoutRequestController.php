<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\MemberPayout;
use App\Http\Requests\PayoutRequest;

class PayoutRequestController extends Controller
{
    public function index()
    {
        $payouts = MemberPayout::where('member_id', auth()->id())->whereNull('confirmed_at')->first();
        return $payouts;
    }

    public function store(PayoutRequest $request)
    {
        $valid = $request->validated();
        $valid['requested_at'] = date('Y-m-d H:i:s');
        $payout = auth()->user()->payouts()->create($valid);
        return $payout;
    }
}
