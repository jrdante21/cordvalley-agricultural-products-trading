<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Purchase;

class PurchaseController extends Controller
{
    public function __invoke(Request $request)
    {
        $purchases = Purchase::with('purchaseItems')
            ->where('member_id', $request->member_id)
            ->latestPurchased()
            ->paginate(10);
        return $purchases;
    }
}
