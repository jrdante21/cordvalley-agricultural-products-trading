<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Rules\CheckPassword;
use App\Models\Member;

class UserController extends Controller
{
    public function index()
    {
        $member = Member::withTotalProfit()->withInviter()->find(auth()->id());
        return view('member', compact('member'));
    }
    
    public function updateUsername(Request $request)
    {
        $member = auth()->user();
        $valid = $request->validate([
            'username' => ['required', 'min:5', 'alpha_num', Rule::unique('members', 'username')->ignore($member->id)]
        ]);
        $member->update($valid);
        return $member;
    }

    public function updatePassword(Request $request)
    {
        $member = auth()->user();
        $valid = $request->validate([
            'password_old' => ['required', new CheckPassword($member->password)],
            'password' => 'required|min:5|confirmed|different:password_old',
        ]);

        $member->update(['password' => bcrypt($valid['password'])]);
        return $member;
    }
}
