<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Events\ProfitPurchase;
use App\Http\Requests\PurchaseRequest;
use App\Models\Purchase;
use App\Models\ProductItem;

class PurchaseController extends Controller
{
    public function index(Request $request)
    {
        $purchases = Purchase::with(['purchaseItems','admin'])
            ->when($request->member_id, 
                fn($query, $member_id) => $query->where('member_id', $member_id),
                fn($query) => $query->with('member')
            )
            ->latestPurchased()
            ->paginate(10);
        return $purchases;
    }

    public function store(PurchaseRequest $request)
    {
        $valid = collect($request->validated());
        $items = $valid->pull('purchase_items');
        $purchase = $valid->all();
        $purchase['admin_id'] = auth()->guard('admin')->id();

        $purchaseItems = [];
        foreach ($items as $i) {
            $product = ProductItem::find($i['product_id']);
            $purchaseItems[] = [
                'product_item_id' => $product->id,
                'price' => $product->price_discounted,
                'points' => $product->points,
                'quantity' => $i['quantity']
            ];
        }

        // Save
        try {
            DB::beginTransaction();
            $purchase = Purchase::create($purchase);
            $purchase->purchaseItems()->createMany($purchaseItems);

            event(new ProfitPurchase($purchase));

            DB::commit();
            return $purchase;

        } catch (Throwable $e) {
            DB::rollback();
            return response("Something went wrong ".$e, 500);
        }
    }
}
