<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

use App\Events\ProfitMember;
use App\Http\Requests\MemberRequest;
use App\Models\Code;
use App\Models\Member;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(MemberRequest $request)
    {
        $profile = $request->validated();

        // Find code
        $code = Code::whereNull('member_id')->where('code', $request->code)->first();
        if (empty($code)) return response(['errors' => [
            'code' => ["The code is invalid."]
        ]], 422);
        
        // Find inviter
        $inviter = null;
        if (!empty($request->invited_by)) {
            $inviter = Member::findByAccount($request->invited_by)->first();
            if (empty($inviter)) return response(['errors' => [
                'invited_by' => ["Inviter not found."]
            ]], 422);
            $inviter = $inviter->id;
        }

        // Save
        try {
            DB::beginTransaction();
            // Member
            $profile['account_id'] = $this->generateAccountID();
            $profile['invited_by'] = $inviter;
            $profile['password'] = Hash::make($profile['password']);
            $member = Member::create($profile);

            // Code
            $code->member_id = $member->id;
            $code->save();

            if (!empty($inviter)) event(new ProfitMember($inviter));

            DB::commit();
            return $member;

        } catch (Throwable $e) {
            DB::rollback();
            return response("Something went wrong ".$e, 500);
        }
    }

    private function generateAccountID()
    {
        $account_id = 0;
        do {
            $id = mt_rand(1000000, 9999999);
            $query = Member::where('account_id', $id)->count();
            if ($query <= 0) $account_id = $id;
        } while ($account_id <= 0);
        return $account_id;
    }
}
