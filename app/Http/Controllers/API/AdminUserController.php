<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Rules\AlphaSpace;
use App\Rules\CheckPassword;

class AdminUserController extends Controller
{
    public function updateProfile(Request $request)
    {
        $admin = auth()->guard('admin')->user();
        $valid = $request->validate([
            'fname' => ['required', 'min:2', new AlphaSpace],
            'mname' => ['required', 'min:2', new AlphaSpace],
            'lname' => ['required', 'min:2', new AlphaSpace],
            'username' => ['required', 'min:5', 'alpha_num', Rule::unique('admins', 'username')->ignore($admin->id)]
        ]);
        $admin->update($valid);
        return $admin;
    }

    public function updatePassword(Request $request)
    {
        $admin = auth()->guard('admin')->user();
        $valid = $request->validate([
            'password_old' => ['required', new CheckPassword($admin->password)],
            'password' => 'required|min:5|confirmed|different:password_old',
        ]);

        $admin->update(['password' => bcrypt($valid['password'])]);
        return $admin;
    }
}
