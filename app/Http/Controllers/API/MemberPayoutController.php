<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

use App\Http\Requests\PayoutRequest;
use App\Models\Member;
use App\Models\MemberPayout;

class MemberPayoutController extends Controller
{
    public function index(Request $request)
    {
        $payouts = MemberPayout::with('member')
            ->whereHas('member', fn(Builder $query) => $query->searchByName($request->search)->whereAddress($request))
            ->when($request->pending,
                fn($query) => $query->whereNull('confirmed_at')->get(),
                fn($query) => $query->with('admin')->confirmed()->paginate(20)
            );
        return $payouts;
    }

    public function store(PayoutRequest $request, Member $member)
    {
        $valid = $request->validated();
        $date = date('Y-m-d H:i:s');
        $valid['admin_id'] = auth()->guard('admin')->id();
        $valid['requested_at'] = $date;
        if (empty($request->is_request)) $valid['confirmed_at'] = $date;
        $member->payouts()->create($valid);
        return $valid;
    }
}
