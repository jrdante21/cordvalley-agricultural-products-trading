<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Models\Product;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('superadmin')->except(['index']);
    }

    public function index()
    {
        $products = Product::with('productItems')->get();
        return $products;
    }

    public function store(ProductCreateRequest $request)
    {
        $valid = $request->validated();
        $product = Product::create($valid);
        return $product;
    }

    public function update(ProductUpdateRequest $request, Product $product)
    {
        $valid = $request->validated();
        $product->update($valid);
        return $product;
    }
}
