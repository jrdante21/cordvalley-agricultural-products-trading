<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Member;
use App\Models\MemberPayout;

class MemberPayoutRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('superadmin')->except(['index']);
    }

    public function index(Member $member)
    {
        $payouts = $member->payouts()->whereNull('confirmed_at')->first();
        return $payouts;
    }

    public function update(MemberPayout $payoutRequest)
    {
        $data['confirmed_at'] = date('Y-m-d H:i:s');
        $data['admin_id'] = auth()->guard('admin')->id();
        $payoutRequest->update($data);
        return $payoutRequest;
    }

    public function destroy(MemberPayout $payoutRequest)
    {
        if (!is_null($payoutRequest->confirmed_at)) abort(404);
        $payoutRequest->delete();
    }
}
