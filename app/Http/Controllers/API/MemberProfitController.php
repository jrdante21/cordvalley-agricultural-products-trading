<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MemberProfit;
use App\Models\MemberPayout;

class MemberProfitController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if (empty($request->member_id)) abort(404);

        $profits = MemberProfit::unionMemberPayout($request->member_id)
            ->orderByDesc('date_type')
            ->paginate(20);

        foreach ($profits as $p) {
            $profit = MemberProfit::selectTotalByMember()
                ->whereMemberId($p->member_id)
                ->whereRaw("CONCAT(date, '-', type) <= ?", [$p->date_type])
                ->first();

            $payout = MemberPayout::selectTotalByMember()
                ->whereMemberId($p->member_id)
                ->confirmed()
                ->whereDate('confirmed_at', '<=', $p->date)
                ->first();

            $p->total_profit = (empty($profit)) ? 0 : $profit->total;
            $p->total_payout = (empty($payout)) ? 0 : $payout->total;
            $p->total_balance = $p->total_profit - $p->total_payout;
        }

        return $profits;
    }
}
