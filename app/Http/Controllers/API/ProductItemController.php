<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\ProductItemRequest;
use App\Models\Product;
use App\Models\ProductItem;

class ProductItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('superadmin')->except(['index']);
    }

    public function index()
    {
        $items = ProductItem::with('product')->orderBy('product_id')->get();
        return $items;
    }

    public function store(ProductItemRequest $request, Product $product)
    {
        $valid = $request->validated();
        $product->productItems()->create($valid);
        return $valid;
    }

    public function update(ProductItemRequest $request, ProductItem $productItem)
    {
        $valid = $request->validated();
        $productItem->update($valid);
        return $productItem;
    }
}
