<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\AdminRequest;
use App\Models\Admin;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('superadmin');
    }

    public function index()
    {
        $admins = Admin::withTrashed()->where('is_super', 0)->get();
        return $admins;
    }

    public function store(AdminRequest $request)
    {
        $valid = $request->validated();
        $valid['is_super'] = 0;
        $valid['password'] = bcrypt($valid['password']);
        $admin = Admin::create($valid);
        return $admin;
    }

    public function update(AdminRequest $request, Admin $admin)
    {
        if ($admin->is_super) abort(404);
        $valid = $request->validated();
        if (!empty($valid['password'])) $valid['password'] = bcrypt($valid['password']);
        $admin->update($valid);
        return $admin;
    }

    public function destroy(Admin $admin)
    {
        if ($admin->is_super) abort(404);
        $admin->delete();
        return $admin;
    }

    public function restore($admin)
    {
        $admin = Admin::onlyTrashed()->findOrFail($admin);
        $admin->restore();
        return $admin;
    }
}
