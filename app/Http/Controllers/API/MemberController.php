<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Member;
use App\Models\MemberProfit;

class MemberController extends Controller
{
    public function index(Request $request)
    {
        $members = Member::searchByName($request->search)->whereAddress($request)->paginate(20);
        return $members;
    }

    public function show($member)
    {
        $member = Member::withTotalProfit()->withInviter()->findOrFail($member);
        return $member;
    }
}
