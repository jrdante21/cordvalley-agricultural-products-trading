<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Code;
use App\Models\CodeRemark;

class CodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('superadmin');
    }

    public function index(Request $request)
    {
        $codes = Code::with(['codeRemark', 'member'])
            ->when($request->search, function($query, $search){
                return $query->where('code', 'like', '%'.$search.'%');
            })
            ->orderByRaw('IF(updated_at > created_at, updated_at, created_at) DESC')
            ->paginate(20);

        return $codes;
    }

    public function store(Request $request)
    {
        $valid = $request->validate([
            'number' => 'required|numeric|min:1',
            'remarks' => 'required|min:5'
        ]);

        // Generates codes
        $codes = null;
        do {
            $array = [];
            for ($i=1; $i <= $valid['number']; $i++) {
                $string = collect(str_split('QWERTYUIOPASDFGHJKLZXCVBNM1234567890'))->shuffle()->take(10)->all();
                $array[] = implode($string);
            }
            $count = Code::whereIn('code', $array)->count();
            if ($count <= 0) $codes = $array;

        } while (empty($codes));
        $codes = collect($codes)->map(fn($item) => ['code' => $item])->all();

        // Save
        try {
            DB::beginTransaction();
            $remark = new CodeRemark;
            $remark->admin_id = auth()->guard('admin')->id();
            $remark->remarks = $valid['remarks'];
            $remark->save();
            $remark->codes()->createMany($codes);
            DB::commit();
            return $codes;

        } catch (Throwable $e) {
            DB::rollback();
            return response("Something went wrong ".$e, 500);
        }
    }
}
