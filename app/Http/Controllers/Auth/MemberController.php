<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except(['logout']);
    }

    public function login(Request $request)
    {
        $valid = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if (auth()->attempt($valid)) return auth()->user();
        return response("Username and password is incorrect.", 401);
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->route('login');
    }
}
