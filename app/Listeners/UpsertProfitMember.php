<?php

namespace App\Listeners;

use App\Events\ProfitMember;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Member;
use App\Models\MemberProfit;

class UpsertProfitMember
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\ProfitMember  $event
     * @return void
     */
    public function handle(ProfitMember $event)
    {
        $members = Member::where('invited_by', $event->memberId)->whereDate('created_at', $event->date)->count();
        MemberProfit::updateOrInsert(
            [
                'member_id' => $event->memberId,
                'type' => 1,
                'date' => $event->date,
            ],
            [
                'count' => $members,
                'value' => 500,
            ]
        );
    }
}
