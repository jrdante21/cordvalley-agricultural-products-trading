<?php

namespace App\Listeners;

use App\Events\ProfitPurchase;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\MemberProfit;
use App\Models\Purchase;
use App\Models\Member;

class UpsertProfitPurchase
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\ProfitPurchase  $event
     * @return void
     */
    public function handle(ProfitPurchase $event)
    {
        $member = Member::find($event->purchase->member_id);
        if (!$member->invited_by || empty($member->invited_by)) return false;

        $inviter = Member::find($member->invited_by);
        if (!$inviter) return false;

        $purchase = Purchase::find($event->purchase->id);
        $inviter->profits()->create([
            'type' => 2,
            'count' => 1,
            'value' => $purchase->total_points,
            'date' => $purchase->purchased_at
        ]);
    }

    /*
    public function handle(ProfitPurchase $event)
    {
        $memberId = $event->purchase->member_id;
        $purchases = Purchase::selectRaw("purchased_at, SUM(total) as total")
            ->groupBy('purchased_at')
            ->where('member_id', $memberId)
            ->monthPurchased($event->purchase->purchased_at)
            ->get();

        $value = 200; // Value of every counts
        $divisor = 1900; // Divisor of every total of purchased
        $remainder = 0;
        foreach ($purchases as $p) {
            $dividend = $p['total'] + $remainder;
            MemberProfit::updateOrInsert(
                [
                    'member_id' => $memberId,
                    'type' => 2,
                    'date' => $p['purchased_at'],
                ],
                [
                    'count' => floor($dividend / $divisor),
                    'value' => $value,
                ]
            );

            $remainder = fmod($dividend, $divisor); // Get the remainder and add to the next purchase
        }
    }
    */
}
