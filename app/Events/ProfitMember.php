<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ProfitMember
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $memberId;
    public $date;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($memberId, $date='')
    {
        $this->memberId = $memberId;
        $this->date = (empty($date)) ? date('Y-m-d') : date('Y-m-d', strtotime($date));
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
