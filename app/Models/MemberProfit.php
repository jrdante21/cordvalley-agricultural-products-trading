<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MemberProfit extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $fillable = ['type','count','value','date'];

    public function scopeSelectTotalByMember($query)
    {
        return $query->selectRaw("member_id, SUM(count * value) as total")->groupBy('member_id');
    }

    public function scopeWhereMemberId($query, $id)
    {
        return $query->where('member_id', $id);
    }

    public function scopeUnionMemberPayout($query, $id)
    {
        $payout = MemberPayout::selectRaw("
                member_id,
                profits,
                remarks,
                FALSE as is_profit,
                DATE(confirmed_at) as date,
                CONCAT(DATE(confirmed_at), '-', 3) as date_type
            ")
            ->where('member_id', $id)
            ->confirmed();

        return $query->selectRaw("
            member_id,
            (count * value) as profits,
            (CASE WHEN type = 1 
                THEN 'Invites profit'
                ELSE 'Purchased profit' 
            END) as remarks,
            TRUE as is_profit,
            date,
            CONCAT(date, '-', type) as date_type
        ")
        ->union($payout)
        ->where('member_id', $id);
    }
}
