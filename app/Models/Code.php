<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    use HasFactory;
    protected $fillable = ['code'];

    public function codeRemark()
    {
        return $this->belongsTo(CodeRemark::class)->with('admin');
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }
}
