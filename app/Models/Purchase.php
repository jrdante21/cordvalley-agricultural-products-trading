<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Purchase extends Model
{
    use HasFactory;

    protected $fillable = ['member_id','admin_id','remarks','ar_number','purchased_at'];
    protected $appends = ['total_format'];

    protected static function booted()
    {
        static::addGlobalScope('totalPurchase', function (Builder $builder) {
            $total = PurchaseItem::selectRaw("
                    purchase_id as purchase_total_id,
                    SUM(price * quantity) as total,
                    SUM(points * quantity) as total_points
                ")
                ->groupBy('purchase_id');

            $builder->joinSub($total, 'purchase_total', function($join){
                return $join->on('purchases.id', '=', 'purchase_total.purchase_total_id');
            });
        });
    }

    public function getTotalFormatAttribute()
    {
        return number_format($this->total);
    }

    public function purchaseItems()
    {
        return $this->hasMany(PurchaseItem::class)->withProductName();
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function scopeLatestPurchased($query)
    {
        return $query->orderByRaw("CONCAT(purchased_at, '-', id) DESC");
    }

    public function scopeMonthPurchased($query, $date='')
    {
        $date = (empty($date)) ? date('Y-m') : date('Y-m', strtotime($date));
        return $query->whereRaw("DATE_FORMAT(purchased_at, '%Y-%m') = ?", [$date]);
    }
}
