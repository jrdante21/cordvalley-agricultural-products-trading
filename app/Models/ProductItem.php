<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductItem extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $fillable = ['volume','price','price_discounted','points'];
    protected $appends = ['price_format','price_discounted_format'];

    public function getPriceFormatAttribute()
    {
        return number_format($this->price);
    }

    public function getPriceDiscountedFormatAttribute()
    {
        return number_format($this->price_discounted);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
