<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    
    protected $fillable = ['title','description'];

    public function productItems()
    {
        return $this->hasMany(ProductItem::class);
    }
}
