<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    protected $hidden = ['password'];
    protected $appends = ['fullname', 'halfname'];
    protected $fillable = ['fname', 'mname', 'lname', 'username', 'password', 'is_super'];
    protected $casts = [
        'is_super' => 'boolean'
    ];

    public function getFullnameAttribute()
    {
        return trim("{$this->fname} {$this->mname} {$this->lname}");
    }

    public function getHalfnameAttribute()
    {
        return trim("{$this->fname} {$this->lname}");
    }

}
