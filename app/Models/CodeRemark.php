<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CodeRemark extends Model
{
    use HasFactory;

    public function codes()
    {
        return $this->hasMany(Code::class)->with('member');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
