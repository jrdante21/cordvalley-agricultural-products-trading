<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseItem extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $fillable = ['product_item_id','price','points','quantity'];
    protected $appends = ['total','price_format'];

    public function getTotalAttribute()
    {
        return number_format($this->price * $this->quantity);
    }

    public function getPriceFormatAttribute()
    {
        return number_format($this->price);
    }

    public function scopeWithProductName($query)
    {
        $productItem = ProductItem::selectRaw("
                product_items.id as product_item_id,
                product_items.volume as product_volume,
                products.title as product_title
            ")
            ->join('products', 'product_items.product_id', '=', 'products.id');

        return $query
        ->joinSub($productItem, 'product_items', function($join){
            $join->on('purchase_items.product_item_id', '=', 'product_items.product_item_id');
        });
    }
}
