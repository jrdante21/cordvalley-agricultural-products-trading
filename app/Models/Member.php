<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class Member extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $hidden = ['password'];
    protected $fillable = [
        'account_id','invited_by',
        'fname','mname','lname','gender','bday','suffix','contact_number',
        'barangay','municipality','province',
        'username','password'
    ];
    protected $appends = ['fullname','address','gender_str'];

    public function getFullnameAttribute()
    {
        return Str::title(trim("{$this->fname} {$this->mname} {$this->lname} {$this->suffix}"));
    }

    public function getAddressAttribute()
    {
        return Str::title("{$this->barangay}, {$this->municipality}, {$this->province}");
    }

    public function getGenderStrAttribute()
    {
        return ($this->gender <= 1) ? 'Male' : 'Female';
    }

    public function code()
    {
        return $this->hasOne(Code::class);
    }

    public function payouts()
    {
        return $this->hasMany(MemberPayout::class);
    }

    public function profits()
    {
        return $this->hasMany(MemberProfit::class);
    }

    public function scopeFindByAccount($query, $account='')
    {
        return $query->where('account_id', $account)->orWhere('username', $account);
    }

    public function scopeWhereAddress($query, $request)
    {
        return $query
        ->when($request->province, fn($query, $province) => $query->where('province', $province))
        ->when($request->municipality, fn($query, $municipality) => $query->where('municipality', $municipality))
        ->when($request->barangay, fn($query, $barangay) => $query->where('barangay', $barangay));
    }

    public function scopeSearchByName($query, $search='')
    {
        if (empty($search)) return $query;
        $search = '%'.$search.'%';
        return $query
        ->whereRaw("
            CONCAT(fname, ' ', mname, ' ', lname) LIKE ? OR
            CONCAT(fname, ' ', lname) LIKE ? OR
            CONCAT(lname, ' ', fname) LIKE ?
        ", [$search, $search, $search]);
    }

    public function scopeWithTotalProfit($query)
    {   
        $profit = MemberProfit::selectTotalByMember();
        $payout = MemberPayout::selectTotalByMember();
        $total = Member::selectRaw("
                members.id as total_id,
                IFNULL(profit.total, 0) as total_profit,
                IFNULL(payout.total, 0) as total_payout,
                (IFNULL(profit.total, 0) - IFNULL(payout.total, 0)) as total_balance
            ")
            ->leftJoinSub($profit, 'profit', fn($join) => $join->on('members.id', '=', 'profit.member_id'))
            ->leftJoinSub($payout, 'payout', fn($join) => $join->on('members.id', '=', 'payout.member_id'));

        return $query->leftJoinSub($total, 'total', fn($join) => $join->on('members.id', '=', 'total.total_id'));
    }

    public function scopeWithInviter($query)
    {
        $member = Member::selectRaw("
                id as inviter_id,
                CONCAT(fname, ' ', mname, ' ', lname, IFNULL(suffix, '')) as inviter_name
            ");

        return $query->leftJoinSub($member, 'inviter', fn($join) => $join->on('members.invited_by', '=', 'inviter.inviter_id'));
    }
}
