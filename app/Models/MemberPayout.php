<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemberPayout extends Model
{
    use HasFactory;

    protected $fillable = ['profits','remarks','admin_id','confirmed_at','requested_at'];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function scopeConfirmed($query)
    {
        return $query->whereNotNull('confirmed_at');
    }

    public function scopeSelectTotalByMember($query)
    {
        return $query->selectRaw("member_id, SUM(profits) as total")->whereNotNull('confirmed_at')->groupBy('member_id');
    }
}
