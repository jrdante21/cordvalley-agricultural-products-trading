<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_items')->delete();
        
        \DB::table('product_items')->insert(array (
            0 => 
            array (
                'id' => 1,
                'points' => '30.00',
                'price' => '650.00',
                'price_discounted' => '550.00',
                'product_id' => 2,
                'volume' => '1 liter',
            ),
            1 => 
            array (
                'id' => 2,
                'points' => '200.00',
                'price' => '2500.00',
                'price_discounted' => '1900.00',
                'product_id' => 2,
                'volume' => '1 gallon',
            ),
        ));
        
        
    }
}