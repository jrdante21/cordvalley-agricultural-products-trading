<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'created_at' => '2022-02-14 11:17:01',
                'description' => 'Fertilizers',
                'id' => 2,
                'title' => 'Power Yield',
                'updated_at' => '2022-02-16 11:20:20',
            ),
        ));
        
        
    }
}