<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admins')->delete();
        
        \DB::table('admins')->insert(array (
            0 => 
            array (
                'created_at' => NULL,
                'deleted_at' => NULL,
                'fname' => 'Super',
                'id' => 1,
                'is_super' => 1,
                'lname' => 'Admin',
                'mname' => 'Over',
                'password' => '$2y$10$i5ZUqffZio6eaT6JiiqcT.BAtY/VE2UPM0ZxFOSRFpxgDWgGXx6d2',
                'updated_at' => '2022-03-05 13:59:01',
                'username' => 'admin',
            ),
            1 => 
            array (
                'created_at' => NULL,
                'deleted_at' => NULL,
                'fname' => 'Dalandan',
                'id' => 4,
                'is_super' => 1,
                'lname' => 'Admin',
                'mname' => 'Over',
                'password' => '$2y$10$i5ZUqffZio6eaT6JiiqcT.BAtY/VE2UPM0ZxFOSRFpxgDWgGXx6d2',
                'updated_at' => '2022-03-05 13:59:01',
                'username' => 'dalandan',
            ),
        ));
        
        
    }
}