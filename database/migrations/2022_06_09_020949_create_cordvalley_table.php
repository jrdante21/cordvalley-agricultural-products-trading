<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateCordvalleyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('country', 100);
            $table->json('locations');
        });

        Schema::create('admins', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('username', 100)->unique('username');
            $table->string('password', 100);
            $table->string('fname', 100);
            $table->string('mname', 100);
            $table->string('lname', 100);
            $table->boolean('is_super');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('code_remarks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('admin_id');
            $table->string('remarks', 500);
            $table->timestamps();
        });

        Schema::create('codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('member_id')->nullable();
            $table->unsignedBigInteger('code_remark_id');
            $table->string('code', 100)->unique('code');
            $table->timestamps();
        });

        Schema::create('member_payouts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('member_id');
            $table->decimal('profits', 10);
            $table->text('remarks');
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->timestamp('confirmed_at')->nullable();
            $table->timestamp('requested_at')->nullable();
            $table->timestamps();
        });

        Schema::create('member_profits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('member_id');
            $table->unsignedTinyInteger('type');
            $table->integer('count');
            $table->decimal('value', 10);
            $table->date('date');
        });

        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('account_id', 20)->unique('account_id');
            $table->unsignedBigInteger('invited_by')->nullable();
            $table->string('username', 100)->unique('username');
            $table->string('password', 100);
            $table->string('fname', 100);
            $table->string('mname', 100);
            $table->string('lname', 100);
            $table->string('suffix', 5)->nullable();
            $table->unsignedTinyInteger('gender');
            $table->date('bday');
            $table->string('contact_number', 20)->nullable();
            $table->string('barangay', 100);
            $table->string('municipality', 100);
            $table->string('province', 100);
            $table->timestamps();
        });

        Schema::create('product_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id');
            $table->string('volume', 20);
            $table->decimal('price', 10);
            $table->decimal('price_discounted', 10);
            $table->decimal('points', 10);
        });

        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 200);
            $table->text('description');
            $table->timestamps();
        });

        Schema::create('purchase_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('purchase_id');
            $table->unsignedBigInteger('product_item_id');
            $table->decimal('price', 10);
            $table->decimal('points', 10);
            $table->integer('quantity');
        });

        Schema::create('purchases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('member_id');
            $table->unsignedBigInteger('admin_id');
            $table->text('remarks');
            $table->string('ar_number', 20);
            $table->date('purchased_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');

        Schema::dropIfExists('purchase_items');

        Schema::dropIfExists('products');

        Schema::dropIfExists('product_items');

        Schema::dropIfExists('members');

        Schema::dropIfExists('member_profits');

        Schema::dropIfExists('member_payouts');

        Schema::dropIfExists('codes');

        Schema::dropIfExists('code_remarks');

        Schema::dropIfExists('admins');

        Schema::dropIfExists('addresses');
    }
}
