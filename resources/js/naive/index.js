import { create,
    NButton, NCard, NEllipsis, NTag, NIcon, NImage,

    NForm, NFormItem, NInput, NInputGroup, NInputGroupLabel, NInputNumber, NDynamicInput,
    NRadio, NRadioGroup, NSwitch, NProgress, NSelect, NTimePicker, NDatePicker, NCheckbox, NCheckboxGroup,

    NLayout, NLayoutContent, NLayoutFooter, NLayoutHeader, NLayoutSider,
    NPagination, NMenu, NEmpty, NTable, NResult, NTabs, NTab, NTabPane,

    NPopover, NModal, NSpin, NAlert,
    
    NMessageProvider, NDialogProvider, 
} from 'naive-ui'

const naive = create({
    components: [
        NButton, NCard, NEllipsis, NTag, NIcon, NImage,
    
        NForm, NFormItem, NInput, NInputGroup, NInputGroupLabel, NInputNumber, NDynamicInput,
        NRadio, NRadioGroup, NSwitch, NProgress, NSelect, NTimePicker, NDatePicker, NCheckbox, NCheckboxGroup,
    
        NLayout, NLayoutContent, NLayoutFooter, NLayoutHeader, NLayoutSider,
        NPagination, NMenu, NEmpty, NTable, NResult, NTabs, NTab, NTabPane,
    
        NPopover, NModal, NSpin, NAlert,
        
        NMessageProvider, NDialogProvider
    ]
})

export default naive