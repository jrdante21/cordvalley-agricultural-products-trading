import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import naive from '~/naive'

import App from '~/components/App.vue'
const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/register',
            name: 'register',
            component: () => import(/* webpackChunkName: "js/dist/web" */ '~/components/web/Register.vue')
        },{
            path: '/login',
            name: 'login',
            component: () => import(/* webpackChunkName: "js/dist/web" */ '~/components/web/LoginMember.vue')
        }
    ]
})

const app = createApp(App)
app.use(naive)
app.use(router)
app.mount('#app')