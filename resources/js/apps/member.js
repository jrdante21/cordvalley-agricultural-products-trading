import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import { createStore } from 'vuex'
import axios from 'axios'
import naive from '~/naive'

import App from '~/components/App.vue'
import AppLayout from '~/components/member/AppLayout.vue'

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/member',
            component: AppLayout,
            children: [
                {
                    path: '',
                    name: 'member',
                    component: () => import(/* webpackChunkName: "js/dist/member" */ '~/components/member/Profits.vue')
                },{
                    path: 'purchases',
                    name: 'member-purchases',
                    component: () => import(/* webpackChunkName: "js/dist/member" */ '~/components/member/Purchases.vue')
                }
            ]
        }
    ]
})

const api = {
    install: (vue) => {
        const $axios = axios.create({ baseURL: '/api-member' })
        $axios.interceptors.response.use(r => { return r }, error => {
            switch (error.response.status) {
                case 401:
                    window.location.replace('/login')
                    break;
            
                case 403:
                    window.$messageAlert.error("You can't do this action.", "Unauthorized action")
                    break;
        
                case 500:
                    window.$messageAlert.error("Try again later.", "Something went wrong")
                    break;
            }
        
            return Promise.reject(error)
        })
        vue.config.globalProperties.$axios = $axios
    }
}

const store = createStore({
    state() {
        return {
            user: window.$user
        }
    },
    mutations: {
        setUser(state, data) {
            state.user = data
        }
    }
})

const app = createApp(App)
app.use(store)
app.use(api)
app.use(naive)
app.use(router)
app.mount('#app')