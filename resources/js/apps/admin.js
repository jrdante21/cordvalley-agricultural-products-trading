import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import { createStore } from 'vuex'
import axios from 'axios'
import naive from '~/naive'

import App from '~/components/App.vue'
import AppLayout from '~/components/admin/AppLayout.vue'
import Member from '~/components/admin/Member.vue'
const prefix = '/capt-admin'
const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: prefix,
            name: 'login',
            component: () => import(/* webpackChunkName: "js/dist/admin" */ '~/components/admin/AppLogin.vue')
        },{
            path: prefix + '/panel',
            component: AppLayout,
            children: [
                {
                    path: 'home',
                    name: 'home',
                    component: () => import(/* webpackChunkName: "js/dist/admin" */ '~/components/admin/Home.vue')
                },{
                    path: 'codes',
                    name: 'codes',
                    component: () => import(/* webpackChunkName: "js/dist/admin" */ '~/components/admin/Codes.vue'),
                    beforeEnter: () => {
                        if (!window.$admin.is_super) return {name:'members'}
                    }
                },{
                    path: '',
                    name: 'members',
                    component: () => import(/* webpackChunkName: "js/dist/admin" */ '~/components/admin/Members.vue')
                },{
                    path: 'member/:id',
                    props: true,
                    component: Member,
                    children: [
                        {
                            path: '',
                            name: 'member',
                            component: () => import(/* webpackChunkName: "js/dist/admin" */ '~/components/admin/MemberProfit.vue')
                        },{
                            path: 'purchases',
                            name: 'member-purchases',
                            component: () => import(/* webpackChunkName: "js/dist/admin" */ '~/components/admin/MemberPurchases.vue')
                        }
                    ]
                },{
                    path: 'payouts',
                    name: 'payouts',
                    component: () => import(/* webpackChunkName: "js/dist/admin" */ '~/components/admin/Payouts.vue')
                },{
                    path: 'purchases',
                    name: 'purchases',
                    component: () => import(/* webpackChunkName: "js/dist/admin" */ '~/components/admin/Purchases.vue')
                },{
                    path: 'products',
                    name: 'products',
                    component: () => import(/* webpackChunkName: "js/dist/admin" */ '~/components/admin/Products.vue')
                },{
                    path: 'admins',
                    name: 'admins',
                    component: () => import(/* webpackChunkName: "js/dist/admin" */ '~/components/admin/Users.vue'),
                    beforeEnter: () => {
                        if (!window.$admin.is_super) return {name:'members'}
                    }
                }
            ]
        }
    ]
})
router.beforeEach((to) => {
    const name = to.name.split('-').map(e => e.charAt(0).toUpperCase() + e.slice(1)).join(' ')
    document.title = 'Admin | '+name
})

const api = {
    install: (vue) => {
        const $axios = axios.create({ baseURL: '/api-admin' })
        $axios.interceptors.response.use(r => { return r }, error => {
            switch (error.response.status) {
                case 401:
                    window.location.replace('/capt-admin')
                    break;
            
                case 403:
                    window.$messageAlert.error("You can't do this action.", "Unauthorized action")
                    break;
        
                case 500:
                    window.$messageAlert.error("Try again later.", "Something went wrong")
                    break;
            }
        
            return Promise.reject(error)
        })
        vue.config.globalProperties.$axios = $axios
    }
}

const store = createStore({
    state() {
        return {
            admin: window.$admin,
            payoutRequests: []
        }
    },
    mutations: {
        setAdmin(state, data) {
            state.admin = data
        },
        setPayoutRequests(state, data) {
            state.payoutRequests = data
        },
        removePayoutRequest(state, id) {
            state.payoutRequests = state.payoutRequests.filter(el => el.id != id)
        }
    }
})

const app = createApp(App)
app.use(store)
app.use(api)
app.use(naive)
app.use(router)
app.mount('#app')