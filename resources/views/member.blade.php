@extends('layouts.app')
@section('title', 'CAPT Member')
@section('content')
    <div id="app"></div>
    <script>
        window.$user = @json($member);
    </script>
@endsection
@section('scripts')
    <script src="{{ mix('js/member.js') }}" defer></script>   
@endsection