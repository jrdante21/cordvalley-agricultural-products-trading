@extends('layouts.app')
@section('title', 'Admin')
@section('content')
    <div id="app"></div>
    @auth('admin')
        <script>
            window.$admin = @json(auth()->guard('admin')->user());
        </script>
    @endauth
@endsection
@section('scripts')
    <script src="{{ mix('js/admin.js') }}" defer></script>   
@endsection