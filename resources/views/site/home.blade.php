@extends('layouts.app2')
@section('title', 'Home')
@section('seo')
    @include('includes.seo', [
        'title' => 'Home',
        'description' => 'Discover a healthy products from our natural resources and help us to grow and build our future.'
    ])
@endsection
@section('content')
<div class="h-screen text-white bg-no-repeat bg-cover bg-center"
    style="padding-top: 82px; background-image: url('/images/bg-image-1.webp')">
    <div class="h-full" style="background: rgba(0,0,0,0.6)">
        <div class="custom-container-tablet text-center h-full flex flex-row justify-center items-center py-20 md:py-10">
            <div class="flex-1 py-10">
                <h2 class="custom-h2 text-lg md:text-xl">Welcome to</h2>
                <h1 class="custom-h1 text-xl md:text-40px text-white">
                    <div class="text-white">
                        <span class="text-50px">Cordvalley</span> <br> Agricultural Products Trading
                    </div>
                </h1>
                <p class="text-xl mb-10">
                    Discover a healthy products from our natural resources and help us to grow and build our future.
                </p>
                <a class="custom-button" href="/products">View Our Products</a>
            </div>
        </div>
    </div>
</div>

<div class="my-32">
    <div class="custom-container">
        <div class="custom-flex">
            <div class="flex-1">
                <div class="custom-fancy-shadow w-full bg-cover bg-no-repeat bg-center h-300px lg:h-400px"
                    style="background-image:url('/images/bg-image-3.webp')">
                </div>
            </div>
            <div class="flex-1 text-center md:text-left">
                <h1 class="custom-h1 text-40px">100% Natural Products</h1>
                <p>
                    Our products are made of effective ingredients from natural resources and thoroughly studied
                    by experts to provide a safe and healthier lifestyle.
                </p>
            </div>
        </div>
    </div>
</div>

<div class="my-32 bg-cover bg-no-repeat bg-fixed" style="background-image: url('/images/bg-image-2.webp');">
    <div class="py-32" style="background: rgba(0,0,0,0.6)">
        <div class="custom-container text-center">
            <h1 class="custom-h1 text-40px"><span class="text-white">Extra Income</span></h1>
            <p class="text-white mx-auto" style="max-width: 768px;">
                We provide opportunities for those who want to do business and increase profits through our effective products
                and our marketing strategies.
            </p>
            <p class="mt-5">
                <a href="/marketing" class="custom-button">Learn More</a>
            </p>
        </div>
    </div>
</div>

<div class="my-32">
    <div class="custom-container text-center">
        <h1 class="custom-h1 text-40px">Featured Products</h1>
        <div class="custom-flex my-10">
            <div class="flex-1">
                <div class="mb-2 p-5 bg-forest-light">
                    <div class="h-300px bg-contain bg-no-repeat bg-center bg-clip-content"
                        style="background-image: url('/images/products/power-yield.png')">
                    </div>
                </div>
                <h2 class="custom-h2 text-2xl text-green-800">Power Yield</h2>
                <p>
                    This is a New Technolgy applied special nutrients with high quality and amazing
                    result to improve grains quality and productivity. It helps in restoring the
                    fertility of the soil. It is also compatible with most insecticide.
                </p>
            </div>
            <div class="flex-1">
                <div class="mb-2 p-5 bg-forest-light">
                    <div class="h-300px bg-contain bg-no-repeat bg-center bg-clip-content"
                        style="background-image: url('/images/products/power-drops.png')">
                    </div>
                </div>
                <h2 class="custom-h2 text-2xl text-green-800">Power Drops</h2>
                <p>
                    It is a safe, non-toxic versatile solution that deeply cleanses, sanitisizes, and is effective
                    for the first aid of open wounds, insect bites, burns, pimples, and other skin allergies. It also
                    serves as anti-fungi.
                </p>
            </div>
        </div>
        <p class="mt-5">
            <a href="/products" class="custom-button">View More Products</a>
        </p>
    </div>
</div>
@endsection