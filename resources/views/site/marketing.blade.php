@extends('layouts.app2')
@section('title', 'Marketing')
@section('seo')
    @include('includes.seo', [
        'title' => 'Marketing',
        'description' => 'Our marketing plan provides you the best skills in building your business to gain more income.'
    ])
@endsection
@section('content')
<div class="bg-green-700 bg-no-repeat bg-cover bg-center" style="padding-top: 82px; background-image:url('/images/bg-image-4.webp')">
    <div class="py-32" style="background: rgba(0,0,0,0.6)">
        <div class="custom-container-tablet text-white text-center">
            <h1 class="custom-h1 text-40px text-white">
                <div class="text-white">Marketing</div>
            </h1>
            <p class="text-xl mb-2">
                Our marketing plan provides you the best skills in building your business to gain more income.
            </p>
        </div>
    </div>
</div>

<div class="my-32">
    <div class="custom-container-tablet">
        <div class="my-10">
            <h1 class="custom-h1 text-40px">CAPT COMMISSIONER QUALIFICATIONS</h1>
            <ol class="list-decimal list-inside text-xl text-left">
                <li>Must be 18 years of age and above.</li>
                <li>Must purchase at least P2,500.00 worth of products.</li>
            </ol>
        </div>
        <div class="my-10">
            <h1 class="custom-h1 text-40px">CAPT COMMISSIONER INCENTIVES AND BENEFITS</h1>
            <ol class="list-decimal list-inside text-xl text-left">
                <li>20% discount in every products.</li>
                <li>10% rebates upon every purchased of P2,000.00 worth of products.</li>
                <li>20% commission in every sponsored membership.</li>
                <li>10% additional incentives in every P20,000.00 accumulated purchased.</li>
            </ol>
        </div>
        <div class="my-10">
            <h1 class="custom-h1 text-40px">EXAMPLE ON HOW TO BECOME A CAPT COMMISSIONER</h1>
            <ol class="list-decimal list-inside text-xl text-left">
                <li>Purchased 1 Gal Power Yield =  P2,500.00 SRP</li>
                <li>Fill out membership form.</li>
                <li>You will be encoded as member of CAPT.</li>
                <li>10% additional incentives in every P20,000.00 accumulated purchased.</li>
            </ol>
        </div>
        <div class="my-10">
            <h1 class="custom-h1 text-40px">HOW CAPT COMMISSIONER INCENTIVES AND BENEFITS WORKS?</h1>
            <ol class="list-decimal list-inside text-xl text-left">
                <li>Next purchased is P2,500.00 SRP less 20% = P2,000.00.</li>
                <li>Your purhased is encoded  and you will receive 10% rebates.</li>
                <li>If your enter a membership of P2,500.00  you will receive 20% CAPT commission.</li>
                <li>If data record will detect your accumulated purchased is P20,000.00 you will receive 10% = P2,000.00.</li>
            </ol>
        </div>
        <h1 class="custom-h1 text-40px">CAPT IS GIVING UP 36% OF ITS SALES TO CAPT COMMISIONER</h1>
    </div>
</div>

@endsection