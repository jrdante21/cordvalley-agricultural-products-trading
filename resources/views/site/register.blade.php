@extends('layouts.app2')
@section('title', 'Register')
@section('seo')
    @include('includes.seo', [
        'title' => 'Register',
        'description' => 'Register to get the best of our benefits.'
    ])
@endsection
@section('content')
<div style="padding-top: 82px;">
    <div class="px-5 pb-32 pt-10">
        <div id="app"></div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="{{ mix('js/web.js') }}" defer></script>
@endsection