@extends('layouts.app2')
@section('title', 'About Us')
@section('seo')
    @include('includes.seo', [
        'title' => 'About Us',
        'description' => 'CORDVALLEY AGRICULTURAL TRADING (CAPT) is a legitimate company registered for its  National Operation thru the Department of 
                Trade and Industry (DTI) with Business Permit issued at Cabarroguis, Quirino and duly accomplished its BIR and all legalities.'
    ])
@endsection
@section('content')
<div class="bg-green-700 bg-no-repeat bg-cover bg-center" style="padding-top: 82px; background-image:url('/images/bg-image-6.webp')">
    <div class="py-32" style="background: rgba(0,0,0,0.6)">
        <div class="custom-container-tablet text-white text-center">
            <h1 class="custom-h1 text-40px text-white">
                <div class="text-white">About Us</div>
            </h1>
            <p class="text-xl mb-2">
                <strong>CORDVALLEY AGRICULTURAL TRADING (CAPT)</strong> is a legitimate company registered for its  National Operation thru the Department of 
                Trade and Industry (DTI) with Business Permit issued at Cabarroguis, Quirino and duly accomplished its BIR and all legalities.
            </p>
            <p class="text-xl mb-2">
                <strong>CAPT</strong> is a direct selling company distributing wholesale, retail agricultural products and others.
            </p>
        </div>
    </div>
</div>

<div class="my-32">
    <div class="custom-container-tablet text-center">
        <div class="my-10">
            <h1 class="custom-h1 text-40px">Our Vision</h1>
            <p class="text-xl">
                Improve rural livelihoods and sustainable food system in the Country.
            </p>
        </div>
        <div class="my-10">
            <h1 class="custom-h1 text-40px">Our Mission</h1>
            <p class="text-xl">
                To promote entrepreneurship through trainings and seminars to farmers and entrepreneurs to 
                increase the yields of all crops in the respective Regions.
            </p>
        </div>
        <div class="my-10">
            <h1 class="custom-h1 text-40px text-center">Our Values</h1>
            <ol class="list-decimal list-inside text-xl text-left">
                <li>
                    Respect and value for local knowledge and innovations.
                </li>
                <li>
                    Promote the importance of organic based products to the environment and ecosystems.
                </li>
                <li>
                    Professionally informed clients by relevant scientific evidence  thru Good Agricultural Practice.
                </li>
                <li>
                    Passion to help farmers and entrepreneurs to earn income for living.
                </li>
            </ol>
        </div>
    </div>
</div>

<div class="my-10">
    <div class="custom-container-tablet text-center">
        <h1 class="custom-h1 text-40px">Reach Out To Us</h1>
        <p class="mb-2 text-xl">
            Don't hesistate to contact us or visit us on details provided below.
        </p>
        <div class="flex flex-wrap justify-center gap-x-5 gap-y-1">
            <a href="tel:09553615446" class="flex items-center gap-3">
                <i class="icon-phone_enabled text-xl w-4 text-center"></i>
                <div class="flex-1">0955-361-5446</div>
            </a>
            <a href="tel:09176280727" class="flex items-center gap-3">
                <i class="icon-phone_enabled text-xl w-4 text-center"></i>
                <div class="flex-1">0917-628-0727</div>
            </a>
            <a href="tel:09386203684" class="flex items-center gap-3">
                <i class="icon-phone_enabled text-xl w-4 text-center"></i>
                <div class="flex-1">0938-620-3684</div>
            </a>
        </div>
        <div class="flex flex-wrap justify-center gap-x-5 gap-y-1">
            <a href="https://goo.gl/maps/PE4eE6qM573xjDRB8" target="_blank" class="flex items-center gap-3">
                <i class="icon-pin_drop text-xl w-4 text-center"></i>
                <div class="flex-1">PK 5 Zamora, Cabarroguis, Quirino</div>
            </a>
            <a href="mailto:dansaweygal@yahoo.com" class="flex items-center gap-3">
                <i class="icon-mail_outline text-xl w-4 text-center"></i>
                <div class="flex-1">dansaweygal@yahoo.com</div>
            </a>
        </div>
    </div>
</div>

<div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2704.9279373502472!2d121.52164285283014!3d16.508632338265198!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33907383ebcdc17f%3A0x31dc7a64b77895e4!2sPaz%20Store!5e0!3m2!1sen!2sph!4v1654729111750!5m2!1sen!2sph" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</div>
@endsection