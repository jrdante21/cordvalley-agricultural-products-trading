@extends('layouts.app2')
@section('title', 'Products')
@section('seo')
    @include('includes.seo', [
        'title' => 'Products',
        'description' => 'Discover our 100% natural products from finest ingredients from nature.'
    ])
@endsection
@section('content')
<div class="bg-green-700 bg-no-repeat bg-cover bg-center" style="padding-top: 82px; background-image:url('/images/bg-image-5.webp')">
    <div class="py-32" style="background: rgba(0,0,0,0.6)">
        <div class="custom-container-tablet text-white text-center">
            <h1 class="custom-h1 text-40px text-white">
                <div class="text-white">Products</div>
            </h1>
            <p class="text-xl mb-2">
                Discover our 100% natural products from finest ingredients from nature.
            </p>
        </div>
    </div>
</div>

<div class="my-32">
    <div class="custom-container">
        <h1 class="custom-h1 text-40px text-center">Flagship Products</h1>
        <div class="custom-flex my-10">
            <div class="flex-1 lg:w-400px lg:flex-initial">
                <div class="p-5 bg-forest-light">
                    <div class="h-300px bg-contain bg-no-repeat bg-center bg-clip-content"
                        style="background-image: url('/images/products/power-yield.png')">
                    </div>
                </div>
            </div>
            <div class="flex-1">
                <h2 class="custom-h2 text-2xl text-green-800">Power Yield</h2>
                <p class="text-xl mb-2">
                    This is a New Technolgy applied special nutrients with high quality and amazing
                    result to improve grains quality and productivity. It helps in restoring the
                    fertility of the soil. It is also compatible with most insecticide.
                </p>
                <p class="text-xl mb-2">
                    Effective in every stage of plant cycle with strong penetration and power to spread that
                    absorbed properly through the leaves and translocate inside the plants and prevent the spread of decease.
                </p>
            </div>
        </div>
        <div class="custom-flex my-10">
            <div class="flex-1 lg:w-400px lg:flex-initial">
                <div class="p-5 bg-forest-light">
                    <div class="h-300px bg-contain bg-no-repeat bg-center bg-clip-content"
                        style="background-image: url('/images/products/power-drops.png')">
                    </div>
                </div>
            </div>
            <div class="flex-1">
                <h2 class="custom-h2 text-2xl text-green-800">Power Drops</h2>
                <p class="text-xl mb-2">
                    It is a safe, non-toxic versatile solution that deeply cleanses, sanitisizes, and is effective
                    for the first aid of open wounds, insect bites, burns, pimples, and other skin allergies. It also
                    serves as anti-fungi. It reduces wrinkles, whiten skin, promotes hair growth, healthy scalp that
                    would prevent dandruff, and eliminates lice.
                </p>
                <p class="text-xl mb-2">
                    This product is a power sanitizer, deodorizer, feminine wash, facial wash, mouthwash, cleanses,
                    whitens teeth, and other beauty and personal hygiene.
                </p>
            </div>
        </div>
    </div>
</div>

<div class="my-32">
    <div class="custom-container text-center">
        <h1 class="custom-h1 text-40px">Other Products</h1>
        <div class="custom-flex my-10">
            <div class="flex-1">
                <div class="mb-2 p-5 bg-forest-light">
                    <div class="h-300px bg-contain bg-no-repeat bg-center bg-clip-content"
                        style="background-image: url('/images/products/power-eye-drops.png')">
                    </div>
                </div>
                <h2 class="custom-h2 text-2xl text-green-800">Power Eye Drops</h2>
            </div>
            <div class="flex-1">
                <div class="mb-2 p-5 bg-forest-light">
                    <div class="h-300px bg-contain bg-no-repeat bg-center bg-clip-content"
                        style="background-image: url('/images/products/power-rub.png')">
                    </div>
                </div>
                <h2 class="custom-h2 text-2xl text-green-800">Power Rub</h2>
            </div>
        </div>
    </div>
</div>
@endsection