@extends('layouts.app')
@section('title', 'Cordvalley')
@section('content')
    <div id="app"></div>
@endsection
@section('scripts')
    <script src="{{ mix('js/web.js') }}" defer></script>
@endsection