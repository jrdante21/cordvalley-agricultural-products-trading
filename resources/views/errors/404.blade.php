@extends('layouts.app2')
@section('title', 'Page Not Found')
@section('content')
<div style="padding-top: 82px;"></div>
<div class="custom-container-tablet">
    <div class="text-center pt-10 pb-20">
        <div class="mb-5">
            <img src="/images/error-404.png" alt="404 Not Found" class="w-full md:w-400px mx-auto">
        </div>
        <h1 class="custom-h1 text-40px">Page Not Found!</h1>
        <p class="mb-5 text-xl">
            Sorry, the page you're looking for doesn't exist. Make sure the URL
            you submitted is correct.
        </p>
        <a href="/" class="custom-button">Go Back Home</a>
    </div>
</div>
@endsection