<meta name="description" content="{{ $description }}">
<meta name="keywords" content="{{ !isset($keywords) ? 'cordvalley, agricultural, products, trading, power drops, mineral drops, healthy products' : $keywords }}">
<meta name="author" content="Danuel Sawey Galgaleng">
<meta property="og:title" content="{{ $title }} | Cordvalley Agricultural Products Trading">
<meta property="og:description" content="{{ $description }}">
<meta property="og:image" content="{{ !isset($image) ? url('/images/logo.png') : $image }}">
<meta name="twitter:title" content="{{ $title }} | Cordvalley Agricultural Products Trading">
<meta name="twitter:description" content="{{ $description }}">
<meta name="twitter:url" content="{{ !isset($image) ? url('/images/logo.png') : $image }}">