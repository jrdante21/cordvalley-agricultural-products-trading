<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="icon" href="/images/logo.png">
    <meta name="naive-ui-style" />
    @yield('seo')
    @yield('scripts')
    <title>@yield('title') | Cordvalley Agricultural Products Trading</title>
</head>
<style>
    .n-layout, .n-layout-header, .n-layout-content {
        background: transparent !important;
    }
    td {
        background-color: transparent !important;
    }
</style>
<body class="bg-blue-50">
    @php
        $active = 'border-b border-green-600 border-solid text-green-600';
    @endphp
    <div class="fixed bg-white shadow-sm w-full z-40">
        <div class="custom-container">
            <div class="flex gap-2 items-center py-4">
                <div><img src="/images/logo.png" alt="logo" width="50"></div>
                <div class="flex-1">
                    <div class="custom-h1 text-xl text-green-700" style="margin: 0px;">Cordvalley</div>
                    <div class="text-sm">Agricultural Products Trading</div>
                </div>
                <div class="hidden lg:block">
                    <div class="flex items-center gap-5 uppercase font-bold text-sm">
                        <a href="/" class="hover:text-green-600 py-1 {{ (request()->is('/')) ? $active : '' }}">Home</a>
                        <a href="/products" class="hover:text-green-600 py-1 {{ (request()->is('products')) ? $active : '' }}">Products</a>
                        <a href="/marketing" class="hover:text-green-600 py-1 {{ (request()->is('marketing')) ? $active : '' }}">Marketing</a>
                        <a href="/about-us" class="hover:text-green-600 py-1 {{ (request()->is('about-us')) ? $active : '' }}">About Us</a>
                        <a href="/register" class="hover:text-green-600 py-1 {{ (request()->is('register')) ? $active : '' }}">Register</a>
                        
                        @if (!request()->is('login'))
                            <div>
                                <a href="/login" class="custom-button">Login</a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="block lg:hidden">
                    <button class="custom-button btnToggleMenu">
                        <i class="icon-menu text-xl"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    @yield('content')
    <div class="py-10 bg-forest-dark text-white">
        <footer class="custom-container">
            <div class="flex flex-col md:flex-row items-center gap-8">
                <div class="text-center">
                    <div class="bg-white inline-block rounded-full">
                        <img src="/images/logo.png" alt="logo" width="100">
                    </div>
                </div>
                <div class="flex-1">
                    <h1 class="custom-h1 text-2xl"><span class="text-white">Cordvalley Agricultural Products Trading</span></h1>
                    <div class="flex flex-col gap-1">
                        <a href="https://goo.gl/maps/PE4eE6qM573xjDRB8" target="_blank" class="flex items-center gap-3">
                            <i class="icon-pin_drop text-xl w-4 text-center"></i>
                            <div class="flex-1">PK 5 Zamora, Cabarroguis, Quirino</div>
                        </a>
                        <div class="flex items-center gap-3">
                            <i class="icon-phone_enabled text-xl w-4 text-center"></i>
                            <div>
                                <a href="tel:09553615446">0955-361-5446</a> / 
                                <a href="tel:09176280727">0917-628-0727</a> / 
                                <a href="tel:09386203684">0938-620-3684</a>
                            </div>
                        </div>
                        <a href="mailto:dansaweygal@yahoo.com" class="flex items-center gap-3">
                            <i class="icon-mail_outline text-xl w-4 text-center"></i>
                            <div class="flex-1">dansaweygal@yahoo.com</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="pt-5 mt-10 border-t border-solid border-slate-700 text-sm text-slate-400 text-center">
                <div class="flex flex-wrap justify-center gap-x-5 gap-y-1">
                    <a href="/" class="hover:text-white">Home</a>
                    <a href="/products" class="hover:text-white">Products</a>
                    <a href="/marketing" class="hover:text-white">Marketing</a>
                    <a href="/about-us" class="hover:text-white">About Us</a>
                    <a href="/register" class="hover:text-white">Register</a>
                    <a href="/privacy-policy" class="hover:text-white">Privacy Policy</a>
                </div>
                <div class="mt-5">
                    &copy; All Rights Reserved @ 2022
                </div>
            </div>
        </footer>
    </div>

    {{-- Mobile Menu --}}
    <div id="divToggleMenu" style="display: none;">
        <div class="animate__animated animate__faster overlay fixed inset-0 z-50 btnToggleMenu" style="background: rgba(0,0,0,0.7);"></div>
        <div class="animate__animated animate__faster content fixed inset-y-0 right-0 bg-white z-50 p-5" style="width: 300px">
            <div class="flex gap-2 items-center mb-6">
                <div><img src="/images/logo.png" alt="logo" width="50"></div>
                <div class="flex-1">
                    <div class="custom-h1 text-xl">Cordvalley</div>
                </div>
                <div>
                    <button class="btnToggleMenu text-gray-700 hover:text-black cursor-pointer px-2 py-px">
                        <i class="icon-clear text-xl"></i>
                    </button>
                </div>
            </div>
            <div class="flex flex-col gap-3 uppercase font-bold">
                <a href="/" class="hover:text-green-600 py-1 {{ (request()->is('/')) ? $active : '' }}">Home</a>
                <a href="/products" class="hover:text-green-600 py-1 {{ (request()->is('products')) ? $active : '' }}">Products</a>
                <a href="/marketing" class="hover:text-green-600 py-1 {{ (request()->is('marketing')) ? $active : '' }}">Marketing</a>
                <a href="/about-us" class="hover:text-green-600 py-1 {{ (request()->is('about-us')) ? $active : '' }}">About Us</a>
                <a href="/register" class="hover:text-green-600 py-1 {{ (request()->is('register')) ? $active : '' }}">Register</a>
                @if (!request()->is('login'))
                @endif
            </div>
            <div class="mt-10">
                <a href="/login" class="custom-button w-full block">Login</a>
            </div>
        </div>
    </div>

    <script>
        const sleep = m => new Promise(r => setTimeout(r, m))
        const btnToggleMenu = document.querySelectorAll('.btnToggleMenu')
        const toogleDiv = async () => {
            const divToggleMenu = document.querySelector('#divToggleMenu')
            const menuOverlay = document.querySelector('#divToggleMenu .overlay')
            const menuContent = document.querySelector('#divToggleMenu .content')

            if (divToggleMenu.style.display == 'none') {
                divToggleMenu.style.display = 'block'
                menuOverlay.classList.add('animate__fadeIn')
                menuContent.classList.add('animate__fadeInRight')
                await sleep(500)
                menuOverlay.classList.remove('animate__fadeIn')
                menuContent.classList.remove('animate__fadeInRight')

            } else {
                menuOverlay.classList.add('animate__fadeOut')
                menuContent.classList.add('animate__fadeOutRight')
                await sleep(500)
                divToggleMenu.style.display = 'none'
                menuOverlay.classList.remove('animate__fadeOut')
                menuContent.classList.remove('animate__fadeOutRight')
            }
        }
        
        btnToggleMenu.forEach(e => {
            e.addEventListener('click', () => toogleDiv())
        })
    </script>
</body>
</html>