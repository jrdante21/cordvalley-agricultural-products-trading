<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="icon" href="/images/logo.png">
    @yield('scripts')
    <meta name="naive-ui-style" />
    <title>@yield('title')</title>
</head>
<style>
    .n-layout, .n-layout-header, .n-layout-content {
        background: transparent !important;
    }
    td {
        background-color: transparent !important;
    }
</style>
<body class="bg-blue-50">
    @yield('content')
</body>
</html>