var contentSizes = {
  '200px': '200px',
  '300px': '300px',
  '350px': '350px',
  '400px': '400px',
}

module.exports = {
  content: [
    // './storage/framework/views/*.php',
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  theme: {
    extend: {
      fontSize: {
        '30px': '30px',
        '40px': '40px',
        '50px': '50px',
      },
      fontFamily: {
        'poppins': ['"Poppins"', 'sans-serif']
      },
      height: contentSizes,
      width: contentSizes,
      colors: {
        'forest-light': '#D7E2CC',
        'forest-dark': '#09260e',
      }
    },
    fontFamily: {
      'sans': ['"Quicksand"', 'sans-serif'],
      'body': ['"Quicksand"', 'sans-serif'],
    }
  },
  plugins: [],
}
